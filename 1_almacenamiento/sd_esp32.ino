/*
 * Módulo microsd card adapter con ESP32
 *  
 * Conexiones
 * SD Card adp. | ESP32
 *    CS       5
 *    SCK      18 (SCK)
 *    MOSI     23
 *    MISO     19  
 *    VCC      V5
 *    GND      GND
 *    
 * mahtlinux@gmail.com
 */

#include "FS.h"
#include "SD.h"
#include <SPI.h>

// Pin chip select módulo para ESP32
#define SD_CS 5

// Variables datos
String mensajeDatos;


void setup(){
  Serial.begin(9600);
// Inicializar tarjeta SD
  SD.begin(SD_CS);  
  if(!SD.begin(SD_CS)) {
    Serial.println("No se pudo iniciar tarjeta SD.");
    return;
  }
  uint8_t tipoTarjeta = SD.cardType();
  if(tipoTarjeta == CARD_NONE) {
    Serial.println("Tarjeta SD no insertada");
    return;
  }
  Serial.println("Iniciando tarjeta SD...");
  if (!SD.begin(SD_CS)) {
    Serial.println("ERROR - Falló inicio tarjeta SD");
    return;
  }
  File archivo = SD.open("/datos.txt");
  if(!archivo) {
    Serial.println("El archivo no existe.");
    Serial.println("Creando archivo...");
    escribirDatos(SD, "/datos.txt", "Columna1, Columna2, ColumnaX \r\n");
  }
  else {
    Serial.println("El archivo ya existe");  
  }
  archivo.close();
}//setup

void loop() {  
  datos2SD();
  delay(5000);
}//loop


/*
 * Funciones escritura en tarjeta SD
 */
// Escribir datos del sensor en tarjeta SD
void datos2SD() {
  mensajeDatos = String("Variable1") + "," +String("Variable2")+ "," +String("VariableX") + "\r\n";
  Serial.print("Datos a añadir: ");
  Serial.println(mensajeDatos);
  incluirDatos(SD, "/datos.txt", mensajeDatos.c_str());
}


//Escribir datos en tarjeta SD
void escribirDatos(fs::FS &fs, const char * path, const char * message) {
  Serial.printf("Escribiendo archivo: %s\n", path);
  File file = fs.open(path, FILE_WRITE);
  if(!file) {
    Serial.println("No se pudo abrir para escribir.");
    return;
  }
  if(file.print(message)) {
    Serial.println("Archivo escrito");
  } else {
    Serial.println("Falló la escritura");
  }
  file.close();
}

//Añadir datos en tarjeta SD
void incluirDatos(fs::FS &fs, const char * path, const char * message) {
  Serial.printf("Añadiendo datos al archivo: %s\n", path);
  File file = fs.open(path, FILE_APPEND);
  if(!file) {
    Serial.println("No se pudo abrir para añadir");
    return;
  }
  if(file.print(message)) {
    Serial.println("Mensaje añadido");
  } else {
    Serial.println("Falló añadir");
  }
  file.close();
}
