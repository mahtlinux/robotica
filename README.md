## Repositorio de programas de robótica
**_Crea y comparte código_** 8-)

En cada categoría, se consideran programas para varias placas: Arduino, ESP32, Micro:bit, etc

### 1. Almacenamiento
+ **Tarjetas SD**
  - Módulo micro SD Apadter con ESP32
    
### 2. Sensores

#### 2.1. Temperatura, humedad, presión atomosférica

+ **BMP280**: _Temperatura, presión atmosférica y altitud_
  - Arduino UNO
  - ESP32  
    - Solo con tarjeta controladora y datos por puerto serie
    - Con función deep sleep (ahorro bateria)
    - Con función deep sleep (ahorro bateria) y función millis para mediciones  
    - Con pantalla OLED 0.96
    - Almacenamiento en SD
    - Almacenamiento en SD, reloj RTC y transmisión con módulo E32 (LORA)
    - Almacenamiento en SD, módulo GPS y transmisión con módulo E32 (LORA)
    - Transmisión de datos via wifi a Thingspeak, con y sin libreria Thingspeak

+ **DS1820**: _Temperatura_
  -ESP32
    - Con pantalla OLED 0.96

+ **DHT22**: _Temperatura y humedad_
  - ESP32
    - Con pantalla OLED 0.96
    - Transmisión de datos via wifi a Thingspeak
#### 2.2. Posición

+ Brújula con Microbit (.hex)
### 2.6. CO<sub>2</sub>

+ **MH-Z19C**: _Medición CO<sub>2</sub> y temperatura_
  - ESP32
    - Con pantall OLED 0.96
    - Transmisión de datos via wifi a Thingspeak

## 3. Robots
+ Luchador de sumo
+ Evitaobstáculos con servos
## 4. Sistemas automáticos
  
+ **Estacion meteorolótica** 

## 5. Comunicaciones

+ **Puertos serie**
  - ESP32
    - Puertos serie hardware
    - Protocolo LORA
## 7. Actuadores

+ **Cámara**
  - ESP32CAM
    - Orden disparo por puerto serie + disparador desde ESP32 con optoacoplador CNY74-2
    - Orden disparo desde programa puerto serie con módulo E32 (LORA) + disparador desde ESP32 con optoacoplador CNY74-2

# Más info y detalles en

[Proyecto Alejandría](https://mahtlinux.gitlab.io/alejandria/3_robotica)

<a href="https://mahtlinux.gitlab.io/alejandria/3_robotica" rel="Proyecto Alejandría"  target="_blank">![Proyecto Alejandria](../imagenes/alejandria.jpg)</a>

# Y aprende todavía más en

<a href="https://codigoeduca.es" rel="mahtlinux"  target="_blank">![mahtlinux](../imagenes/logo_codigoeduca_blue.png)</a>
