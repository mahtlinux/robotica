/*
 * REPOSITORIO DE ROBÓTICA EN
 * mahtlinux.gitlab.io/
 *
 * Descripción programa:
 *   
 * Actuador:   Activación módulo ESP32 CAM a través optoacoplador CNY74-2
 * Tarjeta:    ESP32 que recibe orden a través módulo E32 (LORA)
 *  
 * Visualización datos:   Archivos tajeta SD
 * 
 * Comentarios:
 *  - Pone a cero pin 13 ESP32 CAM conectado a optoacoplador para tomar fotografía
 *
 * Fecha: 8 Enero 2022
 *  
 * Cŕeditos:
 *  - mahtlinux@gmail.org
 * 
 * Especificaciones:
 *   
 * Conexiones  
 * 
 * ESP32
 * - Las especificadas en la declaración de los pines
 * 
 * CNY74-2   | ESP32
 *    PIN1    R220 conectada a GPIO33 ESP32
 *    PIN2    GND
 *    PIN7    R10K conectada a GPIO13 ESP32 CAM
 *    PIN8    GND
 *  
 *  Módulo E32 | ESP32
 *    M0          GND (Es emisor y receptor)
 *    M1          GND
 *    RX          Pin TX elegido de los tres posibles
 *    TX          Pin RX elegido de los tres posibles
 *    AUX         Sin conectar  
 *    VCC         5V
 *    GND         GND   
 *  
 */
     
 //Pines puerto serie
  #define RXD1 4
  #define TXD1 2
  #define RXD2 16
  #define TXD2 17
  #define BAUDRATE 9600

 //Conexión DISPARADOR de prueba 
  const int DISPARADOR = 33;  // Pin digital
  char estado;
    
  void setup() 
  {
    Serial.begin(BAUDRATE);
    Serial1.begin(BAUDRATE, SERIAL_8N1,RXD1 ,TXD1);
    Serial2.begin(BAUDRATE, SERIAL_8N1,RXD2 ,TXD2);
    pinMode(DISPARADOR, OUTPUT);
    digitalWrite(DISPARADOR,LOW);
  }
  
  void loop() 
  {
    // Si por el puerto serie llegan datos (RX)
    if (Serial.available()) //Cambiar por el puerto serie elegido
    {           
      // Almaceno el carácter que llega por el puerto serie (RX) 
      estado = Serial.read(); //Cambiar por el puerto serie elegido
            
      // Si es una 'H'
      if (estado == 'H')          
      {
        // Enciendo el DISPARADOR (nivel ALTO)
        digitalWrite(DISPARADOR, HIGH);         
      }     
      
    }
    delay(500);
    digitalWrite(DISPARADOR, LOW);
  }
