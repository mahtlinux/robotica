/*
 * REPOSITORIO DE ROBÓTICA EN
 * mahtlinux.gitlab.io/
 *
 * Descripción programa: Receptor serie UART
 *   
 * Comunicaciones:  Puertos serie (UART) 
 * Tarjeta:         2 x ESP32
 *  
 * Visualización datos:   Puerto serie (Arduino, Cutecom, etc)
 *                        Salida pin output
 * 
 * Comentarios:
 *  - Permite entender como utilizar los tres puertos serie hardware de la tarjeta ESP32
 *  - Este programa se utiliza en dos tarjetas ESP32, una hace de emisor y otra de receptor
 *  - El receptor es el que tiene el led de prueba
 *  - El emisor utiliza sus pines RX y TX para enviar los datos, conectado a cualquiera de los tres puertos serie de la otra tarjeta
 *
 * Fecha: 7 Enero 2022
 *  
 * Cŕeditos:
 *  - mahtlinux@gmail.org
 * 
 * Especificaciones:
 *   
 * Conexiones  
 * 
 * ESP32
 * - Las especificadas en la declaración de los pines
 * 
 *   
 */
 
    
 //Pines puerto serie
  #define RXD1 4
  #define TXD1 2
  #define RXD2 16
  #define TXD2 17
  #define BAUDRATE 9600

 //Conexión LED de prueba 
  const int LED = 5;  // Pin digital para el LED
  char estado;
    
  void setup() 
  {
    Serial.begin(BAUDRATE);
    Serial1.begin(BAUDRATE, SERIAL_8N1,RXD1 ,TXD1);
    Serial2.begin(BAUDRATE, SERIAL_8N1,RXD2 ,TXD2);
    pinMode(LED, OUTPUT);   // Pin digital 5 como salida
    digitalWrite(LED,LOW);
  }
  
  void loop() 
  {
    // Si por el puerto serie llegan datos (RX)
    if (Serial.available()) //Cambiar por el puerto serie elegido
    {           
      // Almaceno el carácter que llega por el puerto serie (RX) 
      estado = Serial.read(); //Cambiar por el puerto serie elegido
            
      // Si es una 'H'
      if (estado == 'H')          
      {
        // Enciendo el LED (nivel ALTO)
        digitalWrite(LED, HIGH);         
      }
      
      // Si es una 'L'
      if (estado == 'L')           
      {
        // Apago el LED (nivel BAJO)
        digitalWrite(LED, LOW);        
      }
    }
    delay(100);
    
  }
