//flotante 5.1 = {51,51,163,64}
//entero 1234 = {210, 4, 0 ,0}

byte f_trama[]= {51,51,163,64};
byte i_trama[]= {0,0,4,210};

byte array_recibido[] = {83,51,51,163,64,0,0,4,210,10};




void setup() {
  Serial.begin(9600);

}

void loop() {
  Serial.print("Flotante:  ");
  //Serial.print(getFloat(f_trama,0));
  Serial.print(getFloat(array_recibido,1));//desde posición 1
  Serial.print("   |   ");

  Serial.print("Entero:  ");
  //Serial.println(getUnsignedLong(i_trama,0));
  Serial.println(getUnsignedLong(array_recibido,5));
  delay(2000);

}

unsigned long getUnsignedLong(byte packet[], byte i){

    unsigned long value=0;
    value = (value * 256) + packet[i];
    value = (value * 256) + packet[i+1];
    value = (value * 256) + packet[i+2];
    value = (value * 256) + packet[i+3];
    return value;
}

float getFloat(byte packet[], byte i){

    union u_tag{
      byte bin[4];
      float num;
    } 
    u;
    u.bin[0] = packet[i];
    u.bin[1] = packet[i+1];
    u.bin[2] = packet[i+2];
    u.bin[3] = packet[i+3];
    return u.num;  
}
