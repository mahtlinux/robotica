  /*
 * REPOSITORIO DE ROBÓTICA EN
 * mahtlinux.gitlab.io/
 *
 * Descripción programa: Emisor serie UART
 *   
 * Comunicaciones:  Puertos serie (UART) 
 * Tarjeta:         2 x ESP32
 *  
 * Visualización datos:   Puerto serie (Arduino, Cutecom, etc)
 *                        Salida pin output
 * 
 * Comentarios:
 *  - Permite entender como utilizar los tres puertos serie hardware de la tarjeta ESP32
 *  - Este programa se utiliza en dos tarjetas ESP32, una hace de emisor y otra de receptor
 *  - El receptor es el que tiene el led de prueba
 *  - El emisor utiliza sus pines RX y TX para enviar los datos, conectado a cualquiera de los tres puertos serie de la otra tarjeta
 *
 * Fecha: 7 Enero 2022
 *  
 * Cŕeditos:
 *  - mahtlinux@gmail.org
 * 
 * Especificaciones:
 *   
 * Conexiones  
 * 
 * ESP32
 * - Las especificadas en la declaración de los pines
 * 
 *   
 */
 
 //Pines puerto serie
  #define RXD1 4
  #define TXD1 2
  #define RXD2 16
  #define TXD2 17
  #define BAUDRATE 9600

 //Pulsador prueba para envío datos
  const int Pulsador = 25;  // Pin digital para el pulsador
  int estadoPulsador = 0;  // Variable para ver el estado del pulsador
  
 
  void setup() 
  {
    Serial.begin(BAUDRATE);
    Serial1.begin(BAUDRATE, SERIAL_8N1,RXD1 ,TXD1);
    Serial2.begin(BAUDRATE, SERIAL_8N1,RXD2 ,TXD2);
        
    pinMode(Pulsador, INPUT);  // Pin digital como entrada   
  }
  
  void loop()
  {
    // Lee y almacena el estado del pulsador
    estadoPulsador = digitalRead(Pulsador);
  
    // Si el pulsador está presionado
    if (estadoPulsador == HIGH) 
    {                                
      Serial.write('H');           // Enviamos 'H' por el puerto serie (TX), seleccionar puerto serie elegido
    } 
    else 
    {                            
      Serial.write('L');           // Enviamos 'L' por el puerto serie (TX), seleccionar puerto serie elegido
    }
   delay(100);
  }		
