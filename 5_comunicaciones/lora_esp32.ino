/*
 * REPOSITORIO DE ROBÓTICA EN
 * mahtlinux.gitlab.io/
 *
 * Descripción programa:
 *   
 * Comunicaciones:  LORA 
 * Tarjeta:         ESP32
 *  
 * Visualización datos:   Puerto serie (Arduino, Cutecom, etc)
 *                        Salida pin output
 * 
 * Comentarios:
 *  - Permite entender como utilizar los tres puertos serie hardware de la tarjeta ESP32 con el protocolo LORA
 *  - Este programa se utiliza una tarjeta ESP32 como receptor, con módulo E32 conectado a uno de esos tres puertos serie.
 *  - El receptor es el que tiene el led de prueba 
 *  - Como emisor utilizamos un módulo E32 con adaptador USB-UART y envíamos datos con programa CUTECOM
 *
 * Fecha: 8 Enero 2022
 *  
 * Cŕeditos:
 *  - mahtlinux@gmail.org
 * 
 * Especificaciones:
 *   
 * Conexiones  
 * 
 * ESP32
 * - Las especificadas en la declaración de los pines
 * 
 *  Módulo E32 | ESP32
 *    M0          GND (Es emisor y receptor)
 *    M1          GND
 *    RX          Pin TX elegido de los tres posibles
 *    TX          Pin RX elegido de los tres posibles
 *    AUX         Sin conectar  
 *    VCC         5V
 *    GND         GND   
 */
 
    
 //Pines puerto serie
  #define RXD1 4
  #define TXD1 2
  #define RXD2 16
  #define TXD2 17
  #define BAUDRATE 9600

 //Conexión LED de prueba 
  const int LED = 33;  // Pin digital para el LED
  char estado;
    
  void setup() 
  {
    Serial.begin(BAUDRATE);
    Serial1.begin(BAUDRATE, SERIAL_8N1,RXD1 ,TXD1);
    Serial2.begin(BAUDRATE, SERIAL_8N1,RXD2 ,TXD2);
    pinMode(LED, OUTPUT);   // Pin digital 5 como salida
    digitalWrite(LED,LOW);
  }
  
  void loop() 
  {
    // Si por el puerto serie llegan datos (RX)
    if (Serial.available()) //Cambiar por el puerto serie elegido
    {           
      // Almaceno el carácter que llega por el puerto serie (RX) 
      estado = Serial.read(); //Cambiar por el puerto serie elegido
            
      // Si es una 'H'
      if (estado == 'H')          
      {
        // Enciendo el LED (nivel ALTO)
        digitalWrite(LED, HIGH);         
      }
      
      // Si es una 'L'
      if (estado == 'L')           
      {
        // Apago el LED (nivel BAJO)
        digitalWrite(LED, LOW);        
      }
    }
    delay(100);
    
  }
