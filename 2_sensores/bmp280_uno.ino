*
 * GY-BM E/P 280 Module
 * Barometric Pressure & Temperature Sensor
 * Basic Test Sketch/Arduino Uno I2C
 * Chip Id: 0x58 / Address: 0x76 (if SDO=0)
 * Refined Sketch: Prepared & Tested by T.K.Hareendran
 * https://www.electroschematics.com/bmp280-diy-project-primer/
 * Traducción por: mahtlinux@disroot.org
 */

#include <Wire.h>
#include <SPI.h>
#include <Adafruit_BMP280.h>

//I2C Interface
#define BMP280_ADDRESS (0x76)
Adafruit_BMP280 bmp;

void setup()
{
  Serial.begin(9600);
  Serial.println("Comienzo mediciones");
  if (!bmp.begin())
  { 
    Serial.println("Error! No BMP Detectado!!!");
    while (1);
  }
}

void loop()
{
    Serial.print("------------BMP 280 -------------\n");
    Serial.print("Temperatura = ");
    Serial.print(bmp.readTemperature());
    Serial.println(" *C");
    Serial.print("Presión = ");
    Serial.print(bmp.readPressure() / 100); // 100 Pa = 1 millibar (Pa = newton por metro cuadrado)
    Serial.println(" mb");
    Serial.print("Approx Altitude = ");
    Serial.print(bmp.readAltitude(1013.25)); // This should be lined up (atmospheric pressure at sea level is 1013 millibars)
    Serial.println(" m");
    Serial.print("--------------------------------\n\n");
    delay(3000);
}
