/*
 * REPOSITORIO DE ROBÓTICA EN
 * mahtlinux.gitlab.io/
 *
 * Sensor:      DS1820
 * Magnitudes:  Temperatura
 * Tarjeta:     ESP32
 *  
 * Visualización datos:   Pantalla OLED 0.96
 * 
 * Conexiones Sensor
 * BS1820   | ESP32
 *    OUT      2
 *    VCC      3V3
 *    GND      GND
 * 
 * Se instalan desde gestor de librerías (en caso de no estar instaladas)
 *  
 * OneWire
 * Dallas Temperature
 * ESP8266 and ESP32 OLED driver for SSD1306 displays
 *
 * Programa original DS1820 por Rui Santos, https://RandomNerdTutorials.com  
 *
 * Conexiones pantalla OLED
 * OLED   | ESP32
 *    SCL      22
 *    SDA      21 
 *    VCC      3V3
 *    GND      GND
 *  
 * Se instalan desde gestor de librerías (en caso de no estar instaladas)
 *  
 * ESP8266 and ESP32 OLED driver for SSD1306 displays
 *
 * Comentado, reorganizado y adaptado por:
 * mahtlinux@gmail.com
 * 
 * Fecha: 21 Diciembre 2021
 * 
 * Comentarios:
 *      - 
 *      -   
 *  
 */

//Sensor Dallas 1820
#include <OneWire.h>
#include <DallasTemperature.h>
//Pin conexión ESP32
#define ONE_WIRE_BUS 2

// Instancia oneWire para comunicarse con otros dispositivos por oneWire
OneWire oneWire(ONE_WIRE_BUS);

// Llamada a la función con paso por referencia
DallasTemperature sensors(&oneWire);

float t;
unsigned long getDataTimer = 0;

//Display oled
#include "SSD1306.h" 
SSD1306  display(0x3c, 21, 22);


void setup()
{
   //Inicio puerto serie
   Serial.begin(9600);
   
   // Inicio DS1820 
   sensors.begin();

   //display
      display.init();
      display.flipScreenVertically();
      display.setFont(ArialMT_Plain_16);
      display.setTextAlignment(TEXT_ALIGN_LEFT);      
}

void loop()
{
  mediciones(); 
  display.display();
  pantalla();
}


void mediciones(){
  if (millis() - getDataTimer >= 2000)
    { 
        sensors.requestTemperatures();
        t = sensors.getTempCByIndex(0);
                
        Serial.print("Temperatura (C): ");                  
        Serial.println(t);
        
        getDataTimer = millis();
    }   
  }

void pantalla(){
    
    display.clear();
    display.drawString(0, 0, "Temp.: " + String(t) + " ºC\t"); 
 }
