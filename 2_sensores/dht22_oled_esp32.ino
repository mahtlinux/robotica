
/*
 * Sensor:      DHT22 
 * Magnitudes:  Temperatura y Humedad
 * Tarjeta:     ESP32
 *  
 * Visualización datos:   Display OLED 0.96"
 *  
 * Conexiones sensor
 * DHT22   | ESP32
 *    DAT      5
 *    VCC      3V3
 *    GND      GND
 *    
 * Se instalan desde gestor de librerías (en caso de no estar instaladas)
 * 
 * Adafruit DHT Sensor Library
 * Adafruit Unified Sensor
 * Adafruit BusIO 
 * 
 *Conexiones pantalla OLED
 * OLED   | ESP32
 *    SCL      22
 *    SDA      21 
 *    VCC      3V3
 *    GND      GND
 *  
 * Se instalan desde gestor de librerías (en caso de no estar instaladas)
 *  
 * Adafruit DHT Sensor Library
 * Adafruit SSD1306
 * Adafruit GFX
 *   
 *  
 * Programa original por Rui Santos, https://randomnerdtutorials.com 
 *  
 * Comentado, reorganizado y adaptado por:
 * mahtlinux@disroot.org
 * 
 * Fecha: 10 Diciembre 2021
 * 
 * Comentarios:
 *      - Utiliza libreria Adafruit para sensor en lugar de driver de ESP32 para OLED
 *      -   
 *  
 */

//Librerías comunicaciones
#include <Wire.h>// Librería para comunicar dispositivos por I2C

//Librerías display
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

//Librerías sensor
#include <Adafruit_Sensor.h>
#include <DHT.h>
#define DHTPIN 5     // Digital pin connected to the DHT sensor

// Descomentar el sensor a utilizar
//#define DHTTYPE  DHT11     // DHT 11
#define DHTTYPE    DHT22     // DHT 22 (AM2302)
//#define DHTTYPE  DHT21     // DHT 21 (AM2301)

DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(115200);

  dht.begin();

  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
    Serial.println(F("Fallo pantalla SSD1306"));
    for(;;);
  }
  delay(2000);
  display.clearDisplay();
  display.setTextColor(WHITE);
}

void loop() {
  delay(5000);

  //Leer temperatura y humedad
  float t = dht.readTemperature();
  float h = dht.readHumidity();
  if (isnan(h) || isnan(t)) {
    Serial.println("Fallon lectura sensor DHT");
  }
  // borrar pantalla
  display.clearDisplay();
  
  // mostrar temperatura
  display.setTextSize(1);
  display.setCursor(0,0);
  display.print("TEMPERATURA: ");
  display.setTextSize(2);
  display.setCursor(0,10);
  display.print(t);
  display.print(" ");
  display.setTextSize(1);
  display.cp437(true);
  display.write(167);
  display.setTextSize(2);
  display.print("C");
  
  // mostrar humedad
  display.setTextSize(1);
  display.setCursor(0, 35);
  display.print("HUMEDAD: ");
  display.setTextSize(2);
  display.setCursor(0, 45);
  display.print(h);
  display.print(" %");

  Serial.println(t);
  
  display.display(); 
}
