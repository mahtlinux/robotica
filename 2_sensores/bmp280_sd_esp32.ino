/*
 * Sensor:      BMP280 
 * Magnitudes:  Temperatura, Presión atmosférica y altitud
 * Tarjeta:     ESP32
 *  
 * Visualización datos:   Almacenamiento en tarjeta SD
 *  
 * Conexiones sensor BMP280
 * BMP280   | ESP32
 *    SCL      22
 *    SDA      21 
 *    VCC      3V3
 *    GND      GND
 *
 * Se instalan desde gestor de librerías (en caso de no estar instaladas)
 * 
 * Adafruit BMP280 Library
 * Adafruit Unified Sensor
 * Adafruit BusIO 
 *
 * Conexiones Módulo microsd card adapter
 * SD Card adp. | ESP32
 *    CS       5
 *    SCK      18 (SCK)
 *    MOSI     23
 *    MISO     19  
 *    VCC      V5
 *    GND      GND
 *    
 * Comentado, reorganizado y adaptado por:
 * mahtlinux@disroot.org
 * 
 * Fecha: 10 Diciembre 2021
 * 
 * Comentarios:
 *      - Utiliza libreria Adafruit para sensor en lugar de driver de ESP32 para OLED
 *      -   
 * 
 */

//Librerías comunicaciones
#include <Wire.h> // Librería para comunicar dispositivos por I2C
#include <SPI.h> // Librería protocolo trasnsferencia datos serie sincronizados

//Sensor
#include <Adafruit_BMP280.h>//Librería sensor 

//I2C Interface
//Modificar archivo Adafruit_BMP280.h
//Establecer #define BMP280_ADDRESS (0x76) #define BMP280_ADDRESS_ALT (0x76)   
#define BMP280_ADDRESS (0x76)
Adafruit_BMP280 bmp;

//Librerias tarjeta
#include "FS.h" //FS.h - file system wrapper - libreria gestión ficheros
#include "SD.h" // Librería para escribir y leer datos en tarjetas SD


// Pin chip select módulo para ESP32
#define SD_CS 5

// Variables datos
String mensajeDatos;
float temperatura,presion,altitud;

void setup(){
  Serial.begin(9600);
// Inicializar tarjeta SD
  SD.begin(SD_CS);  
  if(!SD.begin(SD_CS)) {
    Serial.println("No se pudo iniciar tarjeta SD.");
    return;
  }
  uint8_t tipoTarjeta = SD.cardType();
  if(tipoTarjeta == CARD_NONE) {
    Serial.println("Tarjeta SD no insertada");
    return;
  }
  Serial.println("Iniciando tarjeta SD...");
  if (!SD.begin(SD_CS)) {
    Serial.println("ERROR - Falló inicio tarjeta SD");
    return;
  }
  File archivo = SD.open("/datos.txt");
  if(!archivo) {
    Serial.println("El archivo no existe.");
    Serial.println("Creando archivo...");
    escribirDatos(SD, "/datos.txt", "Temperatura, Presión, Altitud \r\n");
  }
  else {
    Serial.println("El archivo ya existe");  
  }
  archivo.close();
  
// Inicio sensor BMP280
  Serial.println("Comienzo mediciones");
  if (!bmp.begin())
  { 
    Serial.println("¡Error! BMP no Detectado");
    while (1);
  }
delay(5000);
}//setup

void loop() {  
  temperatura=bmp.readTemperature();
  presion=bmp.readPressure()/100;  // 100 Pa = 1 millibar (Pa = newton por metro cuadrado)
  altitud=bmp.readAltitude(1013.25);  // Actualizar este valor a emplazamiento
  datos2SD();
  delay(1000);
}//loop


/*
 * Funciones escritura en tarjeta SD
 */
// Escribir datos del sensor en tarjeta SD
void datos2SD() {
  mensajeDatos = String(temperatura) + "," +String(presion)+ "," +String(altitud) + "\r\n";
  Serial.print("Datos a añadir: ");
  Serial.println(mensajeDatos);
  incluirDatos(SD, "/datos.txt", mensajeDatos.c_str());
}


//Escribir datos en tarjeta SD
void escribirDatos(fs::FS &fs, const char * path, const char * message) {
  Serial.printf("Escribiendo archivo: %s\n", path);
  File file = fs.open(path, FILE_WRITE);
  if(!file) {
    Serial.println("No se pudo abrir para escribir.");
    return;
  }
  if(file.print(message)) {
    Serial.println("Archivo escrito");
  } else {
    Serial.println("Falló la escritura");
  }
  file.close();
}

//Añadir datos en tarjeta SD
void incluirDatos(fs::FS &fs, const char * path, const char * message) {
  Serial.printf("Añadiendo datos al archivo: %s\n", path);
  File file = fs.open(path, FILE_APPEND);
  if(!file) {
    Serial.println("No se pudo abrir para añadir");
    return;
  }
  if(file.print(message)) {
    Serial.println("Mensaje añadido");
  } else {
    Serial.println("Falló añadir");
  }
  file.close();
}
