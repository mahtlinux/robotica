/*
 * REPOSITORIO DE ROBÓTICA EN
 * mahtlinux.gitlab.io/
 *
 * Descripción programa:
 * 
 * Sensor:      MH-Z19C 
 * Magnitudes:  CO2 y temperatura
 * Tarjeta:     ESP32
 *
 * Visualización datos:   Pantalla OLED 0.96
 * 
 * Comentarios:
 *  - 
 *  -  
 *
 * Fecha: 21 Diciembre 2021
 *  
 * Cŕeditos:
 * - Programa original de la librería MH-Z19 por Jonathan Dempsey
 * - Comentado, reorganizado y adaptado por: mahtlinux@gmail.com
 * 
 *  
 * Especificaciones:
 * 
 * Conexiones Sensor
 * MH-Z19C   | ESP32
 *    RX      17 (TX0)
 *    TX      16 (RX0)
 *    VCC      5V
 *    GND      GND
 *  
 * Se instalan desde gestor de librerías (en caso de no estar instaladas)
 * 
 * MH-Z19
 * 
 * Conexiones pantalla OLED
 * OLED   | ESP32
 *    SCL      22
 *    SDA      21 
 *    VCC      3V3
 *    GND      GND
 *  
 * Se instalan desde gestor de librerías (en caso de no estar instaladas)
 *  
 * ESP8266 and ESP32 OLED driver for SSD1306 displays
 *  
 */


//Comunicaciones
#include <Wire.h> // Librería para comunicar dispositivos por I2C
#include <SPI.h> // Librería protocolo trasnsferencia datos serie sincronizados

#define RXD2 16       // Rx pin placa ESP32. Se conecta a pin TX de MHZ19
#define TXD2 17       // Tx pin placa ESP32. Se conecta a pin RX de MHZ19
#define BAUDRATE 9600 // No cambiar

// Sensor CO2
#include "MHZ19.h"                                        

MHZ19 myMHZ19; 
int CO2; 
int8_t t;

unsigned long getDataTimer = 0;

//Display oled
#include "SSD1306.h" 
SSD1306  display(0x3c, 21, 22);


void setup()
{
    Serial.begin(BAUDRATE);
        
    Serial2.begin(BAUDRATE, SERIAL_8N1, RXD2, TXD2);
    myMHZ19.begin(Serial2);
    myMHZ19.autoCalibration();  // Encendida autocalibración (OFF autoCalibration(false))

    //display
      display.init();
      display.flipScreenVertically();
      display.setFont(ArialMT_Plain_16);
      display.setTextAlignment(TEXT_ALIGN_LEFT);      
}

void loop()
{
  mediciones(); 
  display.display();
  pantalla();
}


void mediciones(){
  if (millis() - getDataTimer >= 2000)
    { 
        CO2 = myMHZ19.getCO2(); //CO2 (ppm)
        
        Serial.print("CO2 (ppm): ");                      
        Serial.println(CO2);                                

       
        t = myMHZ19.getTemperature(); // Temperatura en grados Celsius
        Serial.print("Temperature (C): ");                  
        Serial.println(t);                            
  
        getDataTimer = millis();
    }   
  }

  void pantalla(){
    
    display.clear();
    display.drawString(0, 0, "CO2: " + String(CO2) + " ppm\t"); 
    display.drawString(0, 16, "Temp: " + String(t) + " C");
    
    }
