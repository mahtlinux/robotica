/*CANSAT
 * Sensor BMP280 (Temperatura,Presión atmosférica y altitud), módulo RTC DS3231 con ESP32 y tarjeta SD
 *  
 * Conexiones sensor BMP280
 * BMP280   | ESP32
 *    SCL      22
 *    SDA      21 
 *    VCC      3V3
 *    GND      GND
 *
 * Conexiones Módulo microsd card adapter
 * SD Card adp. | ESP32
 *    CS       5
 *    SCK      18 (SCK)
 *    MOSI     23
 *    MISO     19  
 *    VCC      5V
 *    GND      GND
 *
 *Módulo E32 | ESP32
 *    M0          GND (Es emisor y receptor)
 *    M1          GND
 *    RX          TX0
 *    TX          RXO
 *    AUX         Sin conectar  
 *    VCC         5V
 *    GND         GND
 *
 *Módulo RTCDS3231 | ESP32
 *    SCL         22
 *    SDA         21
 *    VCC         3V3
 *    GND         GND
 *
 * mahtlinux@gmai.com
 * (v1.0 Marzo 2021)
 *
 */

//Librerias sensor
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_BMP280.h>

//Libreria RTC
#include "RTClib.h"
RTC_DS3231 rtc;

//I2C Interface
//Modificar archivo Adafruit_BMP280.h
//Establecer #define BMP280_ADDRESS (0x76) #define BMP280_ADDRESS_ALT (0x76)   
#define BMP280_ADDRESS (0x76)
Adafruit_BMP280 bmp;

//Librerias tarjeta

#include "FS.h" //FS.h - file system wrapper - libreria gestión ficheros
#include "SD.h" // Librería para escribir y leer datos en tarjetas SD
#include <SPI.h> // Librería protocolo trasnsferencia datos serie sincronizados

// Pin chip select módulo para ESP32
#define SD_CS 5

// Variables datos
String mensajeDatos;
float temperatura,presion,altitud;
int annee;

void setup(){
  Serial.begin(9600);
// Inicializar tarjeta SD
  SD.begin(SD_CS);  
  if(!SD.begin(SD_CS)) {
    Serial.println("No se pudo iniciar tarjeta SD.");
    return;
  }
  uint8_t tipoTarjeta = SD.cardType();
  if(tipoTarjeta == CARD_NONE) {
    Serial.println("Tarjeta SD no insertada");
    return;
  }
  Serial.println("Iniciando tarjeta SD...");
  if (!SD.begin(SD_CS)) {
    Serial.println("ERROR - Falló inicio tarjeta SD");
    return;
  }
  File archivo = SD.open("/datos.txt");
  if(!archivo) {
    Serial.println("El archivo no existe.");
    Serial.println("Creando archivo...");
    escribirDatos(SD, "/datos.txt", "Año, Mes, Día, Hora, Minuto, Segundo, Temperatura, Presión, Altitud \r\n");
  }
  else {
    Serial.println("El archivo ya existe");  
  }
  archivo.close();
  
// Inicio sensor BMP280
  Serial.println("Comienzo mediciones");
  if (!bmp.begin())
  { 
    Serial.println("¡Error! BMP no Detectado");
    while (1);
  }

// RTC
if (!rtc.begin()) {
      Serial.println(F("No se pudo encontrar mdoulo RTC"));
      while (1);
   }
   // Si se ha perdido la corriente, fijar fecha y hora
   if (rtc.lostPower()) {
      // Fijar a fecha y hora de compilacion
      rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
      
      // Fijar a fecha y hora específica. Por ejemplo, 5 de mayo de 2021 a las 20:00
      // rtc.adjust(DateTime(2021, 5, 5, 20, 0, 0));
   }  
delay(1000);
}//setup

void loop() {  
  temperatura=bmp.readTemperature();
  presion=bmp.readPressure()/100;  // 100 Pa = 1 millibar (Pa = newton por metro cuadrado)
  altitud=bmp.readAltitude(1013.25);
  DateTime ahora = rtc.now();
  datos2SD(ahora);
  delay(1000);
}//loop


/*
 * Funciones escritura en tarjeta SD
 */
// Escribir datos del sensor en tarjeta SD
void datos2SD(DateTime fecha) {
  mensajeDatos = 
    String(fecha.year(),DEC) + "/" + 
    String(fecha.month(),DEC) + "/" +
    String(fecha.day(),DEC) + "," +
    String(fecha.hour(),DEC) + ":" +
    String(fecha.minute(),DEC) + ":" +
    String(fecha.second(),DEC) + "," +
    String(temperatura) + "," +String(presion)+ "," +String(altitud) + "\r\n";
  //Serial.print("Datos a añadir: ");
  Serial.println(mensajeDatos);
  incluirDatos(SD, "/datos.txt", mensajeDatos.c_str());
}


//Escribir datos en tarjeta SD
void escribirDatos(fs::FS &fs, const char * path, const char * message) {
  //Serial.printf("Escribiendo archivo: %s\n", path);
  File file = fs.open(path, FILE_WRITE);
    if(!file) {
    Serial.println("No se pudo abrir para escribir.");
    return;
  }
  if(file.print(message)) {
    //Serial.println("Archivo escrito");
  } else {
    //Serial.println("Falló la escritura");
  }
  file.close();
}

//Añadir datos en tarjeta SD
void incluirDatos(fs::FS &fs, const char * path, const char * message) {
  //Serial.printf("Añadiendo datos al archivo: %s\n", path);
  File file = fs.open(path, FILE_APPEND);
  if(!file) {
    Serial.println("No se pudo abrir para añadir");
    return;
  }
  if(file.print(message)) {
    //Serial.println("Mensaje añadido");
  } else {
    //Serial.println("Falló añadir");
  }
  file.close();
}
