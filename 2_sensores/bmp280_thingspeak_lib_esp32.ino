/*
 * REPOSITORIO DE ROBÓTICA EN
 * mahtlinux.gitlab.io/
 *
 * Descripción programa:
 *   
 * Sensor:      BMP280 
 * Magnitudes:  Temperatura, Presión atmosférica y altitud
 * Tarjeta:     ESP32
 *  
 * Visualización datos:   Thinkspeak via wifi
 * 
 * Comentarios:
 *  - Reconecta si se corta alimentación placa o wifi
 *  - Con libreria Thinkspeak
 *
 * Fecha: 8 Diciembre 2021
 *  
 * Cŕeditos:
 *  - Programa original http://www.esp32learning.com/code/esp32-and-gy-21p-data-to-thingspeak-example.php
 *  - Comentado, reorganizado y adaptado a BMP280 por mahtlinux@gmail.org
 * 
 * Especificaciones:
 *  
 * Conexiones
 * BMP280   | ESP32
 *    SCL      22
 *    SDA      21 
 *    VCC      3V3
 *    GND      GND
 *    
 * Se instalan desde gestor de librerías (en caso de no estar instaladas)
 * 
 * Adafruit BMP280 Library
 * Adafruit Unified Sensor
 * Adafruit BusIO
 * Thingspeak
 *  
 */

//Conexión wifi
#include <WiFi.h>
char ssid[] = "nombre_red";
char pass[] = "clave_red";
WiFiClient client;

//Thinkspeak
#include "ThingSpeak.h"
unsigned long numero_canal = 0000000;//cambiar 
const char * writeAPIKey = "Write API key";    


//ESP32
#define uS_TO_S_FACTOR 1000000  
#define TIME_TO_SLEEP  50


//Librerías comunicaciones
#include <Wire.h> // Librería para comunicar dispositivos por I2C
#include <SPI.h> // Librería protocolo trasnsferencia datos serie sincronizados

//Sensor
#include <Adafruit_BMP280.h>//Librería sensor 

///I2C Interface
//Modificar archivo Adafruit_BMP280.h
//Establecer #define BMP280_ADDRESS (0x76) #define BMP280_ADDRESS_ALT (0x76)
//en lugar #define BMP280_ADDRESS (0x77)
#define BMP280_ADDRESS (0x76)
Adafruit_BMP280 bmp;

unsigned long previousMillis = 0;  
const long interval = 100000;  
float t;
float p;
float a;

void setup() {
  //Puerto serie
  Serial.begin(9600);
  delay(100);
  
  //Wifi   
  int intentos_conexion = 0;
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, pass);
  delay(3000);//En algunas redes para darle tiempo a conectar
   
  while(WiFi.status() != WL_CONNECTED){
    delay(100);
    intentos_conexion++;
    if(intentos_conexion > 30){
      ESP.deepSleep(30000000);
    }
  }
  Serial.println("Conectado a red wifi");

  Serial.println("Comienzo mediciones");
  if (!bmp.begin())
  { 
    Serial.println("¡Error! BMP no Detectado");
    while (1);
  }
  delay(10);
  ThingSpeak.begin(client);
 
  esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
}


void loop() {
  
  // Conectar o reconectar a red wifi
  if(WiFi.status() != WL_CONNECTED){
      Serial.print("Intentando conectar a red wifi: ");
  //Serial.println(SECRET_SSID);
   while(WiFi.status() != WL_CONNECTED){
      WiFi.begin(ssid, pass); 
      Serial.print(".");
      delay(3000);
    }
    Serial.println("\nConectado a red wifi.");  
  }
  
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
      previousMillis = currentMillis;
      t = bmp.readTemperature(); // Obtiene valor temperatura
      p = bmp.readPressure()/100; // 100 Pa = 1 millibar (Pa = newton por metro cuadrado)
      a = bmp.readAltitude(1023.25);// // Actualizar este valor a emplazamiento
        
      if(!isnan(t) && !isnan(p) && !isnan(a))
      {
       envio_servidor(t, p, a);
      }
      else
      {
        Serial.println("Temp: 0 ºC Pres: 0 mbar  Alt: 0 m;");
        t = 0.0;
        p = 0.0;
        a = 0.0;
      } 
  }
}//loop

void envio_servidor(float temp, float pres, float alt)
{
        ThingSpeak.setField(1, temp);
        ThingSpeak.setField(2, pres);
        ThingSpeak.setField(3, alt);


        // Escritura a canal Thingspeak
        int x = ThingSpeak.writeFields(numero_canal, writeAPIKey);
        if(x == 200){
        Serial.println("Exito envio a Thingspeak");
         } else {
          Serial.println("Problema enviando al canal. HTTP error code " + String(x));
          }
 
        delay(20000);
        
        esp_deep_sleep_start();
}
