 /*
 * REPOSITORIO DE ROBÓTICA EN
 * mahtlinux.gitlab.io/
 *
 * Sensor:      BMP280 
 * Magnitudes:  Temperatura, Presión atmosférica y altitud
 * Tarjeta:     ESP32
 *  
 * Visualización datos:   Puerto serie (Arduino, Cutecom, etc)
 *  
 * Conexiones
 * BMP280   | ESP32
 *    SCL      22
 *    SDA      21 
 *    VCC      3V3
 *    GND      GND
 *    
 * Se instalan desde gestor de librerías (en caso de no estar instaladas)
 * 
 * Adafruit BMP280 Library
 * Adafruit Unified Sensor
 * Adafruit BusIO 
 * 
 * Comentado, reorganizado y adaptado por:
 * mahtlinux@gmail.com
 * 
 * Fecha: 7 Diciembre 2021
 * 
 * * Comentarios:
 *      - 
 *      -   
 *  
 */


//Librerías comunicaciones
#include <Wire.h> // Librería para comunicar dispositivos por I2C
#include <SPI.h> // Librería protocolo trasnsferencia datos serie sincronizados

//Sensor
#include <Adafruit_BMP280.h>//Librería sensor 

///I2C Interfaz
//Modificar archivo Adafruit_BMP280.h
//Establecer #define BMP280_ADDRESS (0x76) #define BMP280_ADDRESS_ALT (0x76)
//en lugar #define BMP280_ADDRESS (0x77)
#define BMP280_ADDRESS (0x76)
Adafruit_BMP280 bmp;

void setup()
{
  // Puerto serie
  Serial.begin(9600);
  //Comprobación sensor
  Serial.println("Comienzo mediciones");
  if (!bmp.begin())
  { 
    Serial.println("¡Error! BMP no Detectado");
    while (1);
  }
}

void loop()
{
    Serial.print("------------BMP 280 -------------\n");
    Serial.print("Temperatura = ");
    Serial.print(bmp.readTemperature());
    Serial.println(" *C");
    Serial.print("Presión = ");
    Serial.print(bmp.readPressure() / 100); // 100 Pa = 1 millibar (Pa = newton por metro cuadrado)
    Serial.println(" mb");
    Serial.print("Altitud aprox. = ");
    Serial.print(bmp.readAltitude(1018)); // Actualizar este valor a emplazamiento
    Serial.println(" m");
    Serial.print("--------------------------------\n\n");
    delay(3000);
}
