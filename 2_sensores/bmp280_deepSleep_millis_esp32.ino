/*
 * REPOSITORIO DE ROBÓTICA EN
 * mahtlinux.gitlab.io/
 *
 * Sensor:      BMP280 
 * Magnitudes:  Temperatura, Presión atmosférica y altitud
 * Tarjeta:     ESP32
 * Modo tarjeta: Deep Sleep Mode (ahorro bateria) 
 * 
 * Visualización datos:   Puerto serie (Arduino, Cutecom, etc)
 *  
 * Conexiones
 * BMP280   | ESP32
 *    SCL      22
 *    SDA      21 
 *    VCC      3V3
 *    GND      GND
 *    
 * Se instalan desde gestor de librerías (en caso de no estar instaladas)
 * 
 * Adafruit BMP280 Library
 * Adafruit Unified Sensor
 * Adafruit BusIO 
 * 
 * Comentado, reorganizado y adaptado por:
 * mahtlinux@gmail.com
 * 
 * Fecha: 6 marzo 2022
 * 
 * Comentarios:
 *      - 
 *      -   
 *  
 */

//ESP32 deep sleep mode
#define uS_TO_S_FACTOR 1000000  //Conversion microsegundos a segundos
//Tiempo para entrar en deep sleep
#define TIME_TO_SLEEP  20 //Segundos
//Al despertar vuelve a ejecutarse setup

//Librerías comunicaciones
#include <Wire.h> // Librería para comunicar dispositivos por I2C
#include <SPI.h> // Librería protocolo trasnsferencia datos serie sincronizados

//Sensor
#include <Adafruit_BMP280.h>//Librería sensor 

///I2C Interfaz
//Modificar archivo Adafruit_BMP280.h
//Establecer #define BMP280_ADDRESS (0x76) #define BMP280_ADDRESS_ALT (0x76)
//en lugar #define BMP280_ADDRESS (0x77)
#define BMP280_ADDRESS (0x76)
Adafruit_BMP280 bmp;

//Parámetros mediciones
const long interval = 10000; //10 segundos
unsigned long previousMillis = 0;  

void setup()
{ 
  // Puerto serie
  Serial.begin(9600);
  //Comprobación sensor
  Serial.println("Comienzo mediciones");
  if (!bmp.begin())
  { 
    Serial.println("¡Error! BMP no Detectado");
    while (1);
  }

   esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
}

void loop()
{
  //Se utiliza millis por si mientras queremos ejecutar otras instrucciones
  //mientras está despierto y así se evita utilizar un delay entre mediciones
  //El tiempo real entre mediciones es la suma del sleep+interval
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
        previousMillis = currentMillis;     
        Serial.print("------------BMP 280 -------------\n");
        Serial.print("Temperatura = ");
        Serial.print(bmp.readTemperature());
        Serial.println(" *C");
        Serial.print("Presión = ");
        Serial.print(bmp.readPressure() / 100); // 100 Pa = 1 millibar (Pa = newton por metro cuadrado)
        Serial.println(" mb");
        Serial.print("Altitud aprox. = ");
        Serial.print(bmp.readAltitude(1018)); // Actualizar este valor a emplazamiento
        Serial.println(" m");
        Serial.print("--------------------------------\n\n");
            
    esp_deep_sleep_start();
  }
  
}//loop
