/*
 * REPOSITORIO DE ROBÓTICA EN
 * mahtlinux.gitlab.io/
 *
 * Descripción programa:
 *   
 * Sensor:      BMP280 
 * Magnitudes:  Temperatura, Presión atmosférica y altitud
 * Tarjeta:     ESP32
 *  
 * Visualización datos:   Thinkspeak via wifi
 * 
 * Comentarios:
 *  - Reconecta si se corta alimentación placa
 *  - No reconecta si se corta conexión wifi (a mejorar)
 *  - Sin utilizar libreria Thinkspeak
 *
 * Fecha: 8 Diciembre 2021
 *  
 * Cŕeditos:
 *  - Programa original con DHT22: https://elektronika327.blogspot.com/2019/06/2-esp32-arduino-thingspeak-dht22.html
 *  - Comentado, reorganizado y adaptado a BMP280 por mahtlinux@gmail.org
 * 
 * Especificaciones:
 *  
 * Conexiones
 * BMP280   | ESP32
 *    SCL      22
 *    SDA      21 
 *    VCC      3V3
 *    GND      GND
 *    
 * Se instalan desde gestor de librerías (en caso de no estar instaladas)
 * 
 * Adafruit BMP280 Library
 * Adafruit Unified Sensor
 * Adafruit BusIO
 * 
 */


//Conexión wifi
#include <WiFi.h>
char ssid[] = "nombre_red";
char pass[] = "clave_red";
WiFiClient client;

//Thinkspeak
String apiKey = "Write API Key Thingspeak";    
const char* serverAddress = "api.thingspeak.com";  

//ESP32
#define uS_TO_S_FACTOR 1000000  
#define TIME_TO_SLEEP  50


//Librerías comunicaciones
#include <Wire.h> // Librería para comunicar dispositivos por I2C
#include <SPI.h> // Librería protocolo trasnsferencia datos serie sincronizados

//Sensor
#include <Adafruit_BMP280.h>//Librería sensor 

///I2C Interface
//Modificar archivo Adafruit_BMP280.h
//Establecer #define BMP280_ADDRESS (0x76) #define BMP280_ADDRESS_ALT (0x76)
//en lugar #define BMP280_ADDRESS (0x77)
#define BMP280_ADDRESS (0x76)
Adafruit_BMP280 bmp;

unsigned long previousMillis = 0;  
const long interval = 100000;  
float t;
float p;
float a;

void setup() {
  //Puerto serie
  Serial.begin(9600);
  delay(100);
  
  //Wifi   
  int intentos_conexion = 0;
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, pass);
   
  while(WiFi.status() != WL_CONNECTED){
    delay(100);
    intentos_conexion++;
    if(intentos_conexion > 30){
      ESP.deepSleep(30000000);
    }
  }
  Serial.println("Conectado a red wifi");

  Serial.println("Comienzo mediciones");
  if (!bmp.begin())
  { 
    Serial.println("¡Error! BMP no Detectado");
    while (1);
  }
 
  esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
}


void loop() {
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
      previousMillis = currentMillis;
      t = bmp.readTemperature(); // Obtiene valor temperatura
      p = bmp.readPressure()/100; // 100 Pa = 1 millibar (Pa = newton por metro cuadrado)
      a = bmp.readAltitude(1023.25);// // Actualizar este valor a emplazamiento
        
      if(!isnan(t) && !isnan(p) && !isnan(a))
      {
        Serial.print("Temp: ");
        Serial.print(t, 3);
        Serial.print(" ºC Pres: ");
        Serial.print(p, 3);
        Serial.print(" mbar Alt: ");
        Serial.print(a, 3);
        Serial.println(" m;");
     }
      else
      {
        Serial.println("Temp: 0 ºC Pres: 0 mbar  Alt: 0 m;");
        t = 0.0;
        p = 0.0;
        a = 0.0;
      }
      envio_servidor(t, p, a);
        
  }
}//loop

void envio_servidor(float temp, float pres, float alt)
{
    if(client.connect(serverAddress,80)){
        String datos = apiKey;
        datos += "&field1=";
        datos += String(temp);
        datos += "&field2=";
        datos += String(pres);
        datos += "&field3=";
        datos += String(alt);
   
        Serial.println(datos);
        datos += "\r\n\r\n";
        client.print("POST /update HTTP/1.1\n");
        client.print("Host: api.thingspeak.com\n");
        client.print("Connection: close\n");
        client.print("X-THINGSPEAKAPIKEY: " + apiKey + "\n");
        client.print("Content-Type: application/x-www-form-urlencoded\n");
        client.print("Content-Length: ");
        client.print(datos.length());
        client.print("\n\n");
        client.print(datos);
        delay(500);
  }
  esp_deep_sleep_start();
}
