/*
 * REPOSITORIO DE ROBÓTICA EN
 * mahtlinux.gitlab.io/
 * 
 * Sensor:      BMP280 
 * Magnitudes:  Temperatura, Presión atmosférica y altitud
 * Tarjeta:     ESP32
 *  
 * Visualización datos:   Display OLED 0.96"
 *  
 * Conexiones Sensor
 * BMP280   | ESP32
 *    SCL      22
 *    SDA      21 
 *    VCC      3V3
 *    GND      GND
 *    
 * Se instalan desde gestor de librerías (en caso de no estar instaladas)
 * 
 * Adafruit BMP280 Library
 * Adafruit Unified Sensor
 * Adafruit BusIO 
 * 
 *Conexiones pantalla OLED
 * OLED   | ESP32
 *    SCL      22
 *    SDA      21 
 *    VCC      3V3
 *    GND      GND
 *  
 * Se instalan desde gestor de librerías (en caso de no estar instaladas)
 *  
 * ESP8266 and ESP32 OLED driver for SSD1306 displays  
 *  
 * Comentado, reorganizado y adaptado por:
 * mahtlinux@gmail.com
 * 
 * Fecha: 7 Diciembre 2021
 * 
 * Comentarios:
 *      - 
 *      -   
 * 
 */

//Librerías comunicaciones
#include <Wire.h> // Librería para comunicar dispositivos por I2C
#include <SPI.h> // Librería protocolo trasnsferencia datos serie sincronizados

//Sensor
#include <Adafruit_BMP280.h>//Librería sensor 

///I2C Interface
//Modificar archivo Adafruit_BMP280.h
//Establecer #define BMP280_ADDRESS (0x76) #define BMP280_ADDRESS_ALT (0x76)
//en lugar #define BMP280_ADDRESS (0x77)
#define BMP280_ADDRESS (0x76)
Adafruit_BMP280 bmp;


//Display oled
#include "SSD1306.h" 
SSD1306  display(0x3c, 21, 22);


void setup(){
  // Puerto serie
  Serial.begin(9600);

  //Comprobación sensor
  Serial.println("Comienzo mediciones");
  if (!bmp.begin())
  { 
    Serial.println("¡Error! BMP no Detectado");
    while (1);
  }
     
  //display
  display.init();
  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_16);
  display.setTextAlignment(TEXT_ALIGN_LEFT); 
  
}

   
void loop(){
    
  //display
  displayDatos();
  display.display();
  //puerto serie
  
  delay(2000);  
  
}


void displayDatos(){
  
  //Lectura temperatura en grados celsius
  float t = bmp.readTemperature();
  //Lectura presión
  float p = bmp.readPressure()/100;// 100 Pa = 1 millibar (Pa = newton por metro cuadrado)
  //Lectura altitud
  float a = bmp.readAltitude(1018); // Actualizar este valor a emplazamiento.
  
  // Comprobar si falla alguna medida y salir para volver a comprobar
  if (isnan(t) || isnan(p) || isnan(a)){
    display.clear(); // Borrado pantalla
    display.drawString(5,0, "Fallo BMP280");
    return;
  }
  display.clear();
  display.drawString(0, 0, "Temp: " + String(t) + " ºC\t"); 
  display.drawString(0, 16, "Pres: " + String(p) + " mbar"); 
  display.drawString(0, 32, "Alt: " + String(a) + " m"); 
}
