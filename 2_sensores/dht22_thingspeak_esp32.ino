
/*
 * Sensor:      DHT22 
 * Magnitudes:  Temperatura y humedad 
 * Tarjeta:     ESP32
 * 
 * Visualización datos:   Thinkspeak via wifi
 *  
 * Conexiones sensor
 * DHT22   | ESP32
 *    DAT      5
 *    VCC      3V3
 *    GND      GND
 *    
 * Se instalan desde gestor de librerías (en caso de no estar instaladas)
 * 
 * Adafruit DHT Sensor Library
 * 
 * 
 * Programa original: https://elektronika327.blogspot.com/2019/06/2-esp32-arduino-thingspeak-dht22.html
 * 
 * Comentado y reorganizado por
 * mahtlinux@disroot.org
 * 
 * 
 * Fecha: 8 Diciembre 2021
 * 
 * * Comentarios:
 *  - Reconecta si se corta alimentación placa
 *  - No reconecta si se corta conexión wifi (a mejorar)
 * 
 */


//Conexión wifi
#include <WiFi.h>
char ssid[] = "nombre_red";
char pass[] = "clave_red";
WiFiClient client;

//Thinkspeak
String apiKey = "Write API Key Thingspeak";    
const char* serverAddress = "api.thingspeak.com";  

//ESP32
#define uS_TO_S_FACTOR 1000000  
#define TIME_TO_SLEEP  50
#define ONE_WIRE_LINE 2      

//Sensor
#include "DHT.h"
#define DHTTYPE DHT22   // DHT 22
uint8_t DHTPin = 5;
DHT dht(DHTPin, DHTTYPE);
unsigned long previousMillis = 0;  
const long interval = 10000;  
float Temperatura;
float Humedad;

void setup() {
  //Puerto serie
  Serial.begin(115200);
  delay(100);
  
  //Sensor
  pinMode(DHTPin, INPUT);
  dht.begin();
  
  //Wifi   
  int intentos_conexion = 0;
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, pass);
   
  while(WiFi.status() != WL_CONNECTED){
    delay(100);
    intentos_conexion++;
    if(intentos_conexion > 30){
      ESP.deepSleep(30000000);
    }
  }
  Serial.println("Conectado a red wifi");
  esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
}


void loop() {
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
      previousMillis = currentMillis;
      Temperatura = dht.readTemperature(); // Obtiene valor temperatura
      Humedad = dht.readHumidity(); // Obtiene valor humedad
      if(!isnan(Temperatura) && !isnan(Humedad))
      {
        Serial.print("Temp: ");
        Serial.print(Temperatura, 3);
        Serial.print(" st.C Humed: ");
        Serial.print(Humedad, 3);
        Serial.println(" %;");
      }
      else
      {
        Serial.println("Temp: 0 st.C Humed: 0%;");
        Temperatura = 0.0;
        Humedad = 0.0;
      }
      envio_servidor(Temperatura, Humedad);
  }
}
void envio_servidor(float temp, float hum)
{
    if(client.connect(serverAddress,80)){
        String datos = apiKey;
        datos += "&field1=";
        datos += String(temp);
        datos += "&field2=";
        datos += String(hum);
   
        Serial.println(datos);
        datos += "\r\n\r\n";
        client.print("POST /update HTTP/1.1\n");
        client.print("Host: api.thingspeak.com\n");
        client.print("Connection: close\n");
        client.print("X-THINGSPEAKAPIKEY: " + apiKey + "\n");
        client.print("Content-Type: application/x-www-form-urlencoded\n");
        client.print("Content-Length: ");
        client.print(datos.length());
        client.print("\n\n");
        client.print(datos);
        delay(500);
  }
  esp_deep_sleep_start();
}
