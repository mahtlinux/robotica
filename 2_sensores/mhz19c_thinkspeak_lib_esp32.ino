/*
 * REPOSITORIO DE ROBÓTICA EN
 * mahtlinux.gitlab.io/
 *
 * Descripción programa:
 *
 * Sensor:      MH-Z19C 
 * Magnitudes:  CO2 y temperatura
 * Tarjeta:     ESP32
 *  
 * Visualización datos:   Thinkspeak via wifi
 * 
 * Comentarios:
 *  - Reconecta si se corta alimentación placa o wifi
 *  - Con libreria Thinkspeak   
 * 
 * Fecha: 26 Diciembre 2021
 * 
 * Cŕeditos:
 * - Programa original Thinkspeak http://www.esp32learning.com/code/esp32-and-gy-21p-data-to-thingspeak-example.php
 * - Programa original de la librería MH-Z19 por Jonathan Dempsey
 * - Comentado, reorganizado y adaptado por: mahtlinux@gmail.com
 * 
 * Especificaciones:
 * 
 * Conexiones Sensor
 * MH-Z19C   | ESP32
 *    RX      17 (TX0)
 *    TX      16 (RX0)
 *    VCC      5V
 *    GND      GND
 *  
 * Se instalan desde gestor de librerías (en caso de no estar instaladas)
 * 
 * MH-Z19
 * Thingspeak 
 *  
 */

//Conexión wifi
#include <WiFi.h>
char ssid[] = "nombre_red";
char pass[] = "clave_red";
WiFiClient client;

//Thinkspeak
#include "ThingSpeak.h"
unsigned long numero_canal = 0000000; //Cambiar 
const char * writeAPIKey = "Write API key";

//ESP32
#define uS_TO_S_FACTOR 1000000  
#define TIME_TO_SLEEP  50

//Comunicaciones
#include <Wire.h> // Librería para comunicar dispositivos por I2C
#include <SPI.h> // Librería protocolo trasnsferencia datos serie sincronizados

#define RXD2 16       // Rx pin placa ESP32. Se conecta a pin TX de MHZ19
#define TXD2 17       // Tx pin placa ESP32. Se conecta a pin RX de MHZ19
#define BAUDRATE 9600 // No cambiar

// Sensor CO2
#include "MHZ19.h"                                        

MHZ19 myMHZ19; 
int CO2; 
int8_t t;

//Parámetros mediciones
unsigned long getDataTimer = 0;


void setup()
{
    //Puerto Serie
    Serial.begin(BAUDRATE);        
    Serial2.begin(BAUDRATE, SERIAL_8N1, RXD2, TXD2);
    myMHZ19.begin(Serial2);
    myMHZ19.autoCalibration();  // Encendido autocalibración (OFF autoCalibration(false))

    //Wifi   
    int intentos_conexion = 0;
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid,pass);
    delay(3000);
   
    while(WiFi.status() != WL_CONNECTED){
      delay(100);
      intentos_conexion++;
      if(intentos_conexion > 30){
        ESP.deepSleep(30000000);
        }
      }
      Serial.println("Conectado a red wifi");
      // Mostrar mensaje de exito y dirección IP asignada
      Serial.println();
      Serial.print("Conectado a:\t");
      Serial.println(WiFi.SSID()); 
      Serial.print("IP address:\t");
      Serial.println(WiFi.localIP()); 

      // Sensores
      Serial.println("Comienzo mediciones");
  
      delay(10);
      ThingSpeak.begin(client);
 
      esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
}

void loop()
{
  // Conectar o reconectar a red wifi
  if(WiFi.status() != WL_CONNECTED){
      Serial.print("Intentando conectar a red wifi: ");
  
   while(WiFi.status() != WL_CONNECTED){
      WiFi.begin(ssid, pass); 
      Serial.print(".");
      delay(5000);
    }
    Serial.println("\nConectado a red wifi.");
    // Mostrar mensaje de exito y dirección IP asignada
    Serial.println();
    Serial.print("Conectado a:\t");
    Serial.println(WiFi.SSID()); 
    Serial.print("IP address:\t");
    Serial.println(WiFi.localIP());     
  }
    
  mediciones();
  
  if(!isnan(CO2) && !isnan(t))
      {
       envio_servidor(CO2, t);
      }
      else
      {
        Serial.println("CO2: 0 ppm Temp: 0 C;");
        CO2 = 0;
        t = 0;        
      }   

}//loop



void mediciones(){
  if (millis() - getDataTimer >= 2000)
    { 
        CO2 = myMHZ19.getCO2(); //CO2 (ppm)
        
        Serial.print("CO2 (ppm): ");                      
        Serial.println(CO2);                                

       
        t = myMHZ19.getTemperature(); // Temperatura en grados Celsius
        Serial.print("Temperature (C): ");                  
        Serial.println(t);                            
  
        getDataTimer = millis();
    }    
}//mediciones 

  
 
void envio_servidor(int dato1, int8_t dato2)
{
        ThingSpeak.setField(1, dato1);
        ThingSpeak.setField(2, dato2);       


        // Escritura a canal Thingspeak
        int x = ThingSpeak.writeFields(numero_canal, writeAPIKey);
        if(x == 200){
        Serial.println("Exito envio a Thingspeak");
         } else {
          Serial.println("Problema enviando al canal. HTTP error code " + String(x));
          }
 
        delay(2000);
        
        esp_deep_sleep_start();
}    
