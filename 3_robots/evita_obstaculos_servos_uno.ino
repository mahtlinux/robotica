/*
 * REPOSITORIO DE ROBÓTICA EN
 * mahtlinux.gitlab.io/
 *
 * Descripción programa:
 * ROBOT EVITAOBSTÁCULOS CON SERVOS
 * 
 * Sensor:      Ultrasonidos HC-SR04 
 * Magnitudes:  Distancia
 * Actuadores:  Servos continuos
 * 
 * Tarjeta:     Arduino UNO
 * 
 * Comentarios:
 *      
 * Fecha: 25 Marzo 2022
 * 
 * Créditos:
 * - mahtlinux@gmail.com
 * 
 * Especificaciones:
 * 
 * Conexiones
 *  Servo 1  | UNO
 *    VCC      5V
 *    GND      GND 
 *    SEÑAL    10
 *   
 *  Servo 2  | UNO
 *    VCC      5V
 *    GND      GND 
 *    SEÑAL    11  
 *   
 *  HC-SR04  | UNO
 *   VCC      5V
 *   GND      GND
 *   TR       7
 *   EC       6
 *   
 *      
 * Se instalan desde gestor de librerías (en caso de no estar instaladas)
 * - Servo
 * 
 *  
 */


//Servos
#include <Servo.h>
Servo derecha;
Servo izquierda;
int vel=50;

//Ultrasonidos
const int triggerEmisor = 7;
const int echoReceptor = 6;
const int valorUmbral = 20;
long tiempoEntrada;  // Almacena el tiempo de respuesta del sensor de entrada
float distanciaEntrada;  // Almacena la distancia en cm a la que se encuentra el objeto
 

void setup()
{
  //Puerto serie
  Serial.begin(9600);
  
  //Servos
  derecha.attach(10);
  izquierda.attach(11);

  //Ultrasonidos
  pinMode(triggerEmisor,OUTPUT); // El emisor emite por lo que es configurado como salida
  pinMode(echoReceptor,INPUT);   // El receptor recibe por lo que es configurado como entrada
  
}

void loop()
{
  sensorUltrasonidos();
  
  if(distanciaEntrada>valorUmbral)
  {
        
    robotAvance();
  }
  else
  {
    robotParar();
     delay(1000);
     robotRetroceso();
     delay(1000);  
    robot180 ();
    delay(2000);
  }
}

void sensorUltrasonidos()
{
    // Se inicializa el sensor de infrasonidos
    digitalWrite(triggerEmisor,LOW);  // Para estabilizar
    delayMicroseconds(10);
 
    // Comenzamos las mediciones
    // Se envía una señal activando la salida trigger durante 10 microsegundos
    digitalWrite(triggerEmisor, HIGH);  // envío del pulso ultrasónico
    delayMicroseconds(10);
    tiempoEntrada=pulseIn(echoReceptor, HIGH); 
    distanciaEntrada= int(0.017*tiempoEntrada); // Fórmula para calcular la distancia en cm
    Serial.println("El valor de la distancia es ");
    Serial.println(distanciaEntrada);
    delay(200);
}

void robotAvance()
{
  // Servo derecha
  derecha.write(0);
  
  // Servo izquierda
  izquierda.write(0);
}

void robotRetroceso()
{
 // Servo derecha
  derecha.write(180);
  
  // Servo izquierda
  izquierda.write(180);
  
}

void robotDerecha()
{
  // Servo derecha
  derecha.write(90);
  
  // Servo izquierda
  izquierda.write(0);  
}

void robotIzquierda ()
{
  // Servo derecha
  derecha.write(0);
  
  // Servo izquierda
  izquierda.write(90);
  
}

void robotParar()
{
  // Servo derecha
  derecha.write(90);
  
  // Servo izquierda
  izquierda.write(90);
}

void robot180 ()
{
 // Servo derecha
  derecha.write(0);
  
  // Servo izquierda
  izquierda.write(180); 
}
