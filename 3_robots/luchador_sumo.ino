//Luchador de sumo
//

//SENSORES IR
int T=A0;
int I=A1;
int D=A2;
int negro=500;
//SOBRE BLANCO=800
//SOBRE NEGRO=7

//TEXTO
String s_ataque;
String s_estado;


//MOTORES
int vel=50;
int vel_ataque=100;
const int pinSTBY = 12;
// Motor izquierdo
const int IN1 = 7; 
const int IN2 = 8;
const int pinPWMA = 6;
//Motor derecho
const int IN3 = 9; 
const int IN4 = 10; 
const int pinPWMB = 11;

//ULTRASONIDOS sensor HCSR04 

const int triggerEmisor = 2;
const int echoReceptor = 3;
const int valorUmbral = 30;
long tiempoEntrada;  
float distanciaEntrada; 
 

void setup()
{
  //IR

  //TEXTO
  s_ataque = String("listo");

  
  //MOTORES
  pinMode(pinSTBY, OUTPUT);
  digitalWrite(pinSTBY, HIGH);
  pinMode (IN1, OUTPUT);
  pinMode (IN2, OUTPUT);
  pinMode (IN3, OUTPUT);
  pinMode (IN4, OUTPUT);
   pinMode(pinPWMA, OUTPUT);
   pinMode(pinPWMB, OUTPUT);

   
 //SENSOR ULTRASONIDOS
  pinMode(triggerEmisor,OUTPUT); 
  pinMode(echoReceptor,INPUT); 
  Serial.begin(9600);

  //POSICION INICIAL COMBATE
{
  delay(3000);
  robotParar();
  while(analogRead(T)>negro){ 
          robotRetroceso();   
       }
  robotParar();
  delay(2000);
} 
}//setup


void loop()
{

if(String(s_ataque).equals(String("listo")) && analogRead(I)>negro && analogRead(D)>negro){ 
  s_estado = String("stop");
  busqueda_ataque();    
  }
  
if(String(s_estado).equals(String("stop")) && analogRead(T)<=negro){
  s_ataque = String("stop");
  robotParar();
  robotAvance(vel);
  delay(1000);
  s_ataque = String("listo");
    
}

if(String(s_estado).equals(String("stop")) && (analogRead(I)<=negro || analogRead(D)<=negro))
{
  s_ataque = String("stop");
  robotParar();
  robotRetroceso();
  delay(1000);
  s_ataque = String("listo");
}
}//loop

//FUNCIONES ULTRASONIDOS
void sensorUltrasonidos()
{
    // Se inicializa el sensor de infrasonidos
    digitalWrite(triggerEmisor,LOW);  // Para estabilizar
    delayMicroseconds(10);
 
    // Comenzamos las mediciones
    // Se envía una señal activando la salida trigger durante 10 microsegundos
    digitalWrite(triggerEmisor, HIGH);  // envío del pulso ultrasónico
    delayMicroseconds(10);
    tiempoEntrada=pulseIn(echoReceptor, HIGH); 
    distanciaEntrada= int(0.017*tiempoEntrada); // Fórmula para calcular la distancia en cm
    Serial.println("El valor de la distancia es ");
    Serial.println(distanciaEntrada);
    delay(200);
}


void busqueda_ataque(){

sensorUltrasonidos();
  
  if(distanciaEntrada>valorUmbral)
  {    
    robotDerecha();
    delay(500);
    robotIzquierda();
    delay(500);
    robotAvance(vel);    
  }
  else
  {    
    robotAvance(vel_ataque);
  }

}

//FUNCIONES MOTORES
void robotAvance(int v)
{
  // Motor izquierdo
  // Al mantener un pin HIGH y el otro LOW el motor gira en un sentido
  digitalWrite (IN1, HIGH);
  digitalWrite (IN2, LOW);
   analogWrite(pinPWMA,v);
  
  // Motor derecho
  // Al mantener un pin HIGH y el otro LOW el motor gira en un sentido
  digitalWrite (IN3, HIGH);
  digitalWrite (IN4, LOW);
   analogWrite(pinPWMB,v);
}

void robotRetroceso()
{
  // Motor izquierdo
  digitalWrite (IN1, LOW);
  digitalWrite (IN2, HIGH);
  analogWrite(pinPWMA,vel);
  // Motor derecho
  digitalWrite (IN3, LOW);
  digitalWrite (IN4, HIGH);
   analogWrite(pinPWMB,vel);
  
}

void robotDerecha()
{
  //  Motor izquierdo
  // Se activa el motor izquierdo
  digitalWrite (IN1, LOW);
  digitalWrite (IN2, HIGH);
   analogWrite(pinPWMA,vel);
  // Motor derecho
  // Se para el motor derecho
  digitalWrite (IN3, LOW);
  digitalWrite (IN4, LOW);
  analogWrite(pinPWMB,0);
}

void robotIzquierda ()
{
   //  Motor izquierdo
  // Se para el motor izquierdo
  digitalWrite (IN1, LOW);
  digitalWrite (IN2, LOW);
  analogWrite(pinPWMA,0);
  // Motor derecho
  // Se activa el motor derecho
  digitalWrite (IN3, LOW);
  digitalWrite (IN4, HIGH);
  analogWrite(pinPWMB,vel);
}

void robotParar()
{
  // Motor izquierdo
  // Se para el motor izquierdo
  digitalWrite (IN1, LOW);
  digitalWrite (IN2, LOW);
  analogWrite(pinPWMA,0);
  // Motor derecho
  // Se para el motor derecho
  digitalWrite (IN3, LOW);
  digitalWrite (IN4, LOW);
  analogWrite(pinPWMB,0);
}

void robot180 ()
{
  // Motor izquierdo
  digitalWrite (IN1, HIGH);
  digitalWrite (IN2, LOW);
   analogWrite(pinPWMA,vel);
  
  // Motor derecho
  digitalWrite (IN3, LOW);
  digitalWrite (IN4, HIGH);
   analogWrite(pinPWMB,vel);
}


void robot0 ()
{
  // Motor izquierdo
  digitalWrite (IN1, LOW);
  digitalWrite (IN2, HIGH);
   analogWrite(pinPWMA,vel);
  
  // Motor derecho
  digitalWrite (IN3, HIGH);
  digitalWrite (IN4, LOW);
   analogWrite(pinPWMB,vel);
}
